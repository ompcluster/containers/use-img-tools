## Setup MPI cluster using Swarm

The script allow yout to deploy containers on the nodes listed in the hostfile (must be reachable by SSH) or locally if no hostfile is provided.

```
Usage: deploy-swarm.sh [OPTIONS] [hostfile]

Options:
-v, --verbose       Print detailed information about the deployement
-h, --help          Show this help message
-i, --image string  Set the image to be deployed (default: brcloudproject/mpitest)
-s, --stop          Stop swarm on all nodes
-n, --number int    Set the number of containers to be deployed (default: node number in hostfile)
```

Valid usage examples:

```sh
./deploy-swarm.sh --image brcloudproject/ubuntu18.04-mpich:latest -n2
./deploy-swarm.sh --image brcloudproject/mpitest -n5 [hostfile]
./deploy-swarm.sh --stop [hostfile]
```

Let us notice that the script does not destroy de swarm after the run. Run the script with the stop flag to destroy the swarm and exit the containers.
