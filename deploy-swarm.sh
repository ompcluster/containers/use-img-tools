#!/bin/bash
#set -x # uncomment to show debug info

#********************** ARGUMENT PARSING *******************
# These switches turn some bugs into errors
# See https://stackoverflow.com/a/29754866 for details
set -o errexit -o pipefail -o noclobber -o nounset

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'Sorry, `getopt --test` failed in this environment.'
    exit 1
fi

# short and longversion of arguments
OPTIONS=vi:hsn:a:
LONGOPTS=verbose,image:,help,stop,number:,advertise-addr:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

imageName="brcloudproject/mpitest"
replicaNumber=0
verbose=n helpFlag=n stopFlag=n
advertiseAddr=""

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -v|--verbose)
            verbose=y
            shift
            ;;
        -h|--help)
            helpFlag=y
            shift
            ;;
        -s|--stop)
            stopFlag=y
            shift
            ;;
        -i|--image)
            imageName="$2"
            shift 2
            ;;
        -n|--number)
            replicaNumber="$2"
            shift 2
            ;;
        -a|--advertise-addr)
            advertiseAddr="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

print_help () {
    cat << EOF
Usage: deploy-swarm.sh [OPTIONS] [hostfile]

Deploy containers on the nodes listed in the hostfile or locally if no hostfile is provided

Options:
-v, --verbose       Print detailed information about the deployement
-h, --help          Show this help message
-i, --image string  Set the image to be deployed (default: brcloudproject/mpitest)
-s, --stop          Stop swarm on all nodes
-n, --number int    Set the number of containers to be deployed (default: node number in hostfile)

Valid usage examples:
$0 --image brcloudproject/ubuntu18.04-mpich:latest -n2
$0 --image brcloudproject/mpitest -n5 [hostfile]
$0 --stop [hostfile]

EOF
}

# cmdSan: a command wrapper and sanitizer
# Removes 'ssh localhost' when applicable
# usage: cmdSan ssh remote_host command_1 command_2 ...
#        cmdSan ssh localhost command_1 command_2 ...
cmdSan() {
    # if we are not using SSH, just exec the function
    if [ "$1" != "ssh" ]; then
        eval "$*"
        return
    fi;

    #if we are using ssh, we now check the hostname
    if [ "$2" != "localhost" ]; then
        #if we are using ssh to a remote host, we keep the call to SSH
        eval "$*"
        return
    fi;

    #since we are running on localhost, we can drop the call to SSH
    shift 2

    eval "$*"

    return
}

# if help flag is set, print help message and then exit
if [[ "$helpFlag" = "y" ]]; then
    print_help
    exit 0
fi

# handle non-option arguments
if [[ $# -gt 1  ]]; then
    echo "$0: Only one non-option argument can be used (the hostname file)."
    print_help
    exit 4
fi

echo "       |      ______  _______  _____          _____  __   __  |"
echo "       |      |     \ |______ |_____] |      |     |   \_/    |"
echo "       |      |_____/ |______ |       |_____ |_____|    |     |"
echo "       |                                                      |"
echo "       |      _______ _  _  _ _______  ______ _______         |"
echo "       |      |______ |  |  | |_____| |_____/ |  |  |         |"
echo "       |      ______| |__|__| |     | |    \_ |  |  |         |"
echo "       |                                                      |"
echo ""


# parse file to get the number and name of host
hostnumber=0
hostnames=""

if [[ $# -eq 1  ]]; then
    # read hostfile
    while read LINE; do
        #skip empty lines
        if [ ! -z "$LINE" -a "$LINE" != " " ]; then
            hostnames="$hostnames $LINE"
            hostnumber=$(($hostnumber + 1))
        fi
    done < $1
else
    # use localhost only
    hostnames="localhost"
    hostnumber=1
fi

# check if the number of replicas set is valid
re='^[0-9]+$'
if ! [[ $replicaNumber =~ $re ]] ; then
    echo "error: Invalid replica number" >&2; exit 4
fi

# set replicas number to host number if not set by the user
if [ $replicaNumber -eq "0" ]; then
    replicaNumber=$hostnumber
fi

# prints meta-info if verbose is set
if [[ "$verbose" = "y" ]]; then
    echo "Number of REPLICA set to = $replicaNumber"
    echo "Image name set to $imageName"
    echo "Stop flag: $stopFlag"
fi


if [[ "$stopFlag" = "y" ]]; then
    echo "[STOP CONTAINERS AND SWARM]"

    i=0
    for hostname in $hostnames; do
        echo "-------------------------------"
        echo "On host: $hostname"

        i=$(($i + 1))

        if [ $i -eq 1 ]; then
            echo "  This is the manager..."
            echo "  Stopping Swarm Service..."
            (cmdSan ssh $hostname 'docker service rm mmpiready09d0' < /dev/null >/dev/null 2>&1 || true)
            echo "  Stopping Overlay network..."
            (cmdSan ssh $hostname 'docker network rm overnet09d0s90ds9' < /dev/null >/dev/null 2>&1 || true)
            echo "  Removing Docker Secret..."
            (cmdSan ssh $hostname 'docker secret rm secret_mmpiready09d0' < /dev/null >/dev/null 2>&1 || true)
        fi

        if $(cmdSan ssh $hostname 'docker swarm leave --force' < /dev/null >/dev/null 2>&1); then
            echo "  This node left the Swarm..."
        fi
    done
    exit
fi

echo "[SPAWN SWARM]"

i=0
for hostname in $hostnames; do
    echo "-------------------------------"
    echo "On host: $hostname"
    echo "  Checking if host is in a swarm..."
    printf "  "
    if cmdSan ssh $hostname 'docker swarm leave --force' < /dev/null > /dev/null 2>&1  ; then
        echo "Left..."
    fi

    echo "  Starting swarm on node..."
    i=$(($i + 1))

    if [ $i -eq 1 ]; then
        echo "  This is the manager..."

        swarmInitReturn=$(cmdSan ssh $hostname 'docker swarm init' < /dev/null 2>&1) || true

        # checks if the first string contains the second
        multiIPErrorMsg="specify one with --advertise-addr"
        if test "${swarmInitReturn#*$multiIPErrorMsg}" != "$swarmInitReturn"; then
            echo "  Multiple IP to advertise on Swarm Manager..."

            # if advertise adress was not set with -a flag
            if [ "$advertiseAddr" == "" ]; then
                echo "  Please set the advertise IP with -a flag."
                echo "  Docker message: $swarmInitReturn"

                exit 1

            # if advertise adress was set, use it
            else
                echo "  Using specified advertising IP..."
                swarmInitReturn=$(cmdSan ssh $hostname 'docker swarm init --advertise-addr ' "$advertiseAddr" < /dev/null 2>&1) || true
            fi
        fi

        auxTokenString=$(echo $swarmInitReturn | grep -e "--")
        joinToken=$(echo $swarmInitReturn | grep "docker swarm join")

        if [ ! -z "$joinToken" -a "$joinToken" != " " ]; then
            echo "  Done..."
        else
            echo "  Something unexpected hapenned..."
        fi

    else
        echo "  This node is a worker..."

        printf "  "
        if cmdSan ssh $hostname "$joinToken" < /dev/null; then
            echo "  Done..."
            printf "  "

        else
            echo "  Something unexpected happened..."
        fi
    fi
    echo ""
done

echo ""
echo "[START SERVICE]"

for hostname in $hostnames; do
    echo "-------------------------------"
    echo "On manager host: $hostname"

    echo "  Trying to exit service before creating one..."

    # it is safe to ignore the non-zero return of the removal
    # commands bellow in most cases
    cmdSan ssh $hostname 'docker network rm overnet09d0s90ds9'  < /dev/null >/dev/null 2>&1 || true
    cmdSan ssh $hostname 'docker service rm mmpiready09d0'  < /dev/null >/dev/null 2>&1 || true

    # if script was interrupted before proprer removal, the old key must be deleted to
    # prevent ssh-keygen from prompting the user
    cmdSan ssh $hostname 'rm -f /tmp/rsa_secret_mmpiready09d0.pub'  < /dev/null #  >/dev/null 2>&1 || true
    cmdSan ssh $hostname 'rm -f /tmp/rsa_secret_mmpiready09d0' < /dev/null # >/dev/null 2>&1 || true

    # generate temporary ssh key
    rsaGenComd="cat /dev/zero | ssh-keygen -q -N '' -f /tmp/rsa_secret_mmpiready09d0"

    cmdSan ssh $hostname $rsaGenComd < /dev/null > /dev/null || true
    cmdSan ssh $hostname 'rm -f /tmp/rsa_secret_mmpiready09d0.pub'  < /dev/null  >/dev/null 2>&1 || true

    createrDockerSecretCmd='cat /tmp/rsa_secret_mmpiready09d0 | docker secret create secret_mmpiready09d0 -'
    cmdSan ssh $hostname $createrDockerSecretCmd < /dev/null > /dev/null
    cmdSan ssh $hostname 'rm -f /tmp/rsa_secret_mmpiready09d0' < /dev/null > /dev/null

    echo "  Starting Overlay network..."
    cmdSan ssh $hostname 'docker network create -d overlay overnet09d0s90ds9' < /dev/null > /dev/null

    echo "  Waiting for service convergence..."

    # phrase service creation command
    serviceArgs='--name mmpiready09d0 --hostname="{{.Service.Name}}--{{.Task.Slot}}"'
    serviceArgs="$serviceArgs --network overnet09d0s90ds9 --secret secret_mmpiready09d0"
    serviceArgs="$serviceArgs --mode replicated --replicas $replicaNumber"
    serviceArgs="$serviceArgs --entrypoint '/usr/local/bin/docker-entrypoint.sh sleep infinity'"

    serviceCreateCommand="docker service create $serviceArgs $imageName"

    # create the service on the manager
    cmdSan ssh $hostname $serviceCreateCommand

    # we only run this from the manager
    break
done

echo ""
echo "[GET CONTAINER HOSTNAME]"

# cleans the file before writing
printf "" >| _CONTAINER_HOSTFILE
printf "" >| _CONTAINER_IP

for hostname in $hostnames; do
    echo "-------------------------------"
    echo "On host: $hostname"
    echo "  Inspecting name..."
    containerId=$(cmdSan ssh $hostname 'docker ps -aqf "name=mmpiready09d0"' < /dev/null)

    # get array of containers
    contArray=($containerId)

    defaultIFS=$IFS #saves default delimiter
    IFS='\n'

    for contId in "${contArray[@]}"
    do
        contIp=$(cmdSan ssh $hostname "docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $contId")
        contHostname=$(cmdSan ssh $hostname "docker inspect --format='{{.Config.Hostname}}' $contId")

        pairIpHostname="$contIp $contId $contHostname "
        printf "$pairIpHostname \n" >> _CONTAINER_IP
    done

    IFS=$defaultIFS #restores delimiter

    printf "$containerId\n" >> _CONTAINER_HOSTFILE
done

echo ""
echo "[INJECT CONTAINER HOSTNAME]"

#replace line breaks with other char
#once inside the container the line break will be restored
containersIpList="$(cat _CONTAINER_IP | tr '\n' '\175')"
containersHostfile="$(cat _CONTAINER_HOSTFILE | tr '\n' ' ')"

for hostname in $hostnames; do
    echo "-------------------------------"
    echo "On host: $hostname"

    # get containers running on this host
    curHostContainersIDs=$(cmdSan ssh $hostname 'docker ps -aqf "name=mmpiready09d0"' < /dev/null)
    curHostContainersIDs=( $curHostContainersIDs )

    # inject file for each container on this host
    for curContainer in "${curHostContainersIDs[@]}"
    do
        printf "  "
        echo "doing for: $curContainer"
        # the first command remove new lines, the second put them back
        $(cmdSan ssh $hostname "docker exec -u 0 $curContainer bash -c 'echo $containersHostfile > /home/mpiuser/hostfile'" < /dev/null)
        $(cmdSan ssh $hostname "docker exec -u 0 $curContainer bash -c 'sed -i \"s/\s\+/\n/g\" /home/mpiuser/hostfile'" < /dev/null)

        $(cmdSan ssh $hostname "docker exec -u 0 $curContainer bash -c 'echo $containersIpList >> /tmp/new_hosts'" < /dev/null)
        $(cmdSan ssh $hostname "docker exec -u 0 $curContainer bash -c 'cat /tmp/new_hosts | tr \"\175\" \"\n\" >> /etc/hosts'" < /dev/null)
    done

    printf "Done...\n"
done

echo ""
echo "[GET MPI EXECUTION COMMAND]"

for hostname in $hostnames; do
    echo "-------------------------------"
    echo "On manager host: $hostname"

    # replace line breaks with commas and remove last char
    allContainers="$(cat _CONTAINER_HOSTFILE | tr '\n' ',' | sed 's/.$//')"
    managerContainer="$(echo $allContainers | cut -d',' -f1 )"
    echo "  Manager container = $managerContainer"
    echo "  Worker containers = $allContainers"

    sshcmd=""
    if [[ "$hostname" != "localhost" ]]; then
        sshcmd="ssh $hostname "
    fi
    echo ""
    echo "Run MPI programs to the cluster using commands like:"
    echo "  ${sshcmd}docker exec -u mpiuser $managerContainer mpirun -host $allContainers /home/mpiuser/a.out"
    echo "Run an interactive terminal on the manager using:"
    echo "  ${sshcmd}docker exec -it -u mpiuser $managerContainer /bin/bash"

    # we only print this from the manager
    break
done

